﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPicker : MonoBehaviour
{
    public Slider red;
    public Slider green;
    public Slider blue;
    public InputField redInputField;
    public InputField greenInputField;
    public InputField blueInputField;
    public Image ImageColor;
    public Dropdown CurrentCilia;
    public uint numberOfPicker;
    public Material Emmissive;
    public Light pointLight;
    private const int RED_OFFSET = 0;
    private const int GREEN_OFFSET = 3;
    private const int BLUE_OFFSET = 6;
    private const int COLOR_SIZE = 3;
    bool readytoupdate = false;
    bool rf = true;
    bool gf = true;
    /*
     * Update color based on change value of the slider
     */
    public void UpdateColor()
    {
        //First make sure that there is an actual Cilia setup.  Shouldn't ever have this happen but check anyways.
        string currentlySelectedCilia = CurrentCilia.GetComponentInChildren<Text>().text;
        if (currentlySelectedCilia.Equals("") != true)
        {
            //make a color from values
            Color tempColor = new Color(red.value, green.value, blue.value);
            //set the color of the little visualizer box near the sliders
            ImageColor.color = tempColor;
            //get an unsigned int version of the value by multiplying our normalized value by 255 and then making it an unsigned int.  will be between 0 and 255
            uint redValue255 = (uint)(red.value * (float)byte.MaxValue);
            uint greenValue255 = (uint)(green.value * (float)byte.MaxValue);
            uint blueValue255 = (uint)(blue.value * (float)byte.MaxValue);
            //get the string version
            redInputField.text = redValue255.ToString();
            greenInputField.text = greenValue255.ToString();
            blueInputField.text = blueValue255.ToString();
            //set the color of the appropriate part of the model as well as the point light
            Emmissive.color = tempColor;
            Emmissive.SetColor("_EmissionColor", tempColor);
            pointLight.color = tempColor;
            //set light of the actual Cilia
            if (rf)
            {
                rf = false;
            }
            else if (gf)
            {
                gf = false;
            }
            else
            {
                Cilia.SetLightSpecificCilia(currentlySelectedCilia, numberOfPicker, redValue255, greenValue255, blueValue255);
                Debug.Log(currentlySelectedCilia + "%%%" + redValue255 + "%%%" + greenValue255 + "%%%" + blueValue255);
            }
        }
    }
    /*
     * Used to set color of color picker based in input fields editing being finished.
     */
    public void UpdateColor2()
    {
        //First make sure that there is an actual Cilia setup.  Shouldn't ever have this happen but check anyways.
        string currentlySelectedCilia = CurrentCilia.GetComponentInChildren<Text>().text;
        if (currentlySelectedCilia.Equals("") != true)
        {
            //Case where field is empty even though we are done editing. Go ahead and set to 0 because that is pretty much what empty means.
            if (redInputField.text.Equals(""))
                redInputField.text = "0";
            if (greenInputField.text.Equals(""))
                greenInputField.text = "0";
            if (blueInputField.text.Equals(""))
                blueInputField.text = "0";
            //Get input value from the text field and if somehow a value outside of range 0-255 made its way in fix it.
            //Then normalize and store the values of the sliders.
            red.value = (float)FixValue(int.Parse(redInputField.text)) / (float)byte.MaxValue;
            green.value = (float)FixValue(int.Parse(greenInputField.text)) / (float)byte.MaxValue;
            blue.value = (float)FixValue(int.Parse(blueInputField.text)) / (float)byte.MaxValue;
        }
    }

    /*
     * Used to correct the values typed in for color.
     * 
     * Make sure users cannot type in negative value or value larger than 255.
     * 
     * Unity is already making sure the values have to be int but we do the rest of the checks
     */
    public void AutoCorrectColor()
    {
        //if there is an active Cilia. Shouldn't need this check but it is a good extra measure.
        string currentlySelectedCilia = CurrentCilia.GetComponentInChildren<Text>().text;
        if (currentlySelectedCilia.Equals("") != true)
        {
            //If empty that means the person pressed backspace. Just return
            if (redInputField.text.Equals(""))
                return;
            if (greenInputField.text.Equals(""))
                return;
            if (blueInputField.text.Equals(""))
                return;
            //get rid of negative symbol since we don't want negative
            redInputField.text = redInputField.text.Replace("-", "");
            greenInputField.text = greenInputField.text.Replace("-", "");
            blueInputField.text = blueInputField.text.Replace("-", "");
            //OK now that we are past the first two checks we need to get or value to make sure it is less than 255
            int redValue = int.Parse(redInputField.text);
            int greenValue = int.Parse(greenInputField.text);
            int blueValue = int.Parse(blueInputField.text);
            //if value is greater than 255 remove the last character by using / by 10.
            if(redValue > byte.MaxValue)
            {
                redValue = redValue / 10;
                redInputField.text = redValue.ToString();
            }
            if(greenValue > byte.MaxValue)
            {
                greenValue = greenValue / 10;
                greenInputField.text = greenValue.ToString();
            }
            if(blueValue > byte.MaxValue)
            {
                blueValue = blueValue / 10;
                blueInputField.text = blueValue.ToString();
            }
        }
    }
    /*
     * Used when we change what Cilia we are looking at.
     * 
     * Takes in a string of what color to set the color picker and sets the color picker accordingly
     */
    public void UpdateColorPicker(string color)
    {
        //Try catch just in case somehow an improper string finds its way in
        try
        {
            //Gets the rgb values from the string using the known offsets and sizes within the color string.
            int redValue = int.Parse(color.Substring(RED_OFFSET, COLOR_SIZE));
            int greenValue = int.Parse(color.Substring(GREEN_OFFSET, COLOR_SIZE));
            int blueValue = int.Parse(color.Substring(BLUE_OFFSET, COLOR_SIZE));
            //Need to normalize the value for the slider.
            float redFValue = (float)redValue / (float)byte.MaxValue;
            float greenFValue = (float)greenValue / (float)byte.MaxValue;
            float blueFValue = (float)blueValue / (float)byte.MaxValue;
            //Set the slider value.
            red.value = redFValue;
            green.value = greenFValue;
            blue.value = blueFValue;
            //Call function to update the color of the actual physical Cilia and the virtual Cilia.
            UpdateColor();
        }
        catch
        {

        }
    }
    /*
     * Useful for fixing an integer between 0 and 255.
     */
    private int FixValue(int input)
    {
        if (input < byte.MinValue)
            input = byte.MinValue;
        if (input > byte.MaxValue)
            input = byte.MaxValue;
        return input;
    }
}

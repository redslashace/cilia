﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Threading;
using UnityEditor;
using System.IO;
using System.IO.MemoryMappedFiles;
using System;
using System.Text;
using System.IO.Pipes;

public class Cilia : MonoBehaviour
{
    bool continueThreads = true;
    protected string oldCilia = "0";
    const int MAX_NUMBER_LOCAL_CILIAS = 256;
    const int NUMBER_OF_SUROUNDPOSITIONS = 8 + 256;
    public int CiliaPort = 1995;
    public string CiliaIP = "localhost";
    static TcpClient CiliaClient = new TcpClient();
    protected static NetworkStream CiliaStream;
    static byte[] message;
    protected static byte[] buffer = new byte[1024];
    static string smellLibraryString = "";
    protected static string[] smellLibrary;
    
    public Dropdown ciliaSelectionDropDown;
    public Dropdown viewGroupDropDown;
    public Dropdown GameProfilesDropDown;
    public Dropdown changeSurroundGroupDropDown;

    public GameObject restartComputer;
    public GameObject models;
    public GameObject noCiliaSelectedText;
    public GameObject uiViewGroup1;
    //public GameObject changeSurroundGroupDropDown;
    public ColorPicker colorPicker1;
    public ColorPicker colorPicker2;
    public ColorPicker colorPicker3;
    public ColorPicker colorPicker4;
    public ColorPicker colorPicker5;
    public ColorPicker colorPicker6;
    public Slider Fan1Slider;
    public Slider Fan2Slider;
    public Slider Fan3Slider;
    public Slider Fan4Slider;
    public Slider Fan5Slider;
    public Slider Fan6Slider;
    public RPMAnimate fanrpm1;
    public RPMAnimate fanrpm2;
    public RPMAnimate fanrpm3;
    public RPMAnimate fanrpm4;
    public RPMAnimate fanrpm5;
    public RPMAnimate fanrpm6;

    protected static List<int>[] ciliaPositions = new List<int>[NUMBER_OF_SUROUNDPOSITIONS];
    protected static List<string>[] ciliaPositionsS = new List<string>[NUMBER_OF_SUROUNDPOSITIONS];
    protected static List<string> ciliaProfilesL = new List<string>();
    protected static string[][] ciliaContents = new string[MAX_NUMBER_LOCAL_CILIAS][];

    protected static List<Dropdown> smells = new List<Dropdown> { };

    public Dropdown Smell1;
    public Dropdown Smell2;
    public Dropdown Smell3;
    public Dropdown Smell4;
    public Dropdown Smell5;
    public Dropdown Smell6;

    public Text LaunchFakeCiliaBText;

    public SmellLibrary smellLibraryClass;

    public bool timeForUpdate = false;

    StreamReader streamReader;

    public string[] surroundPositions = { "Front Center", "Front Left", "Side Left", "Rear Left", "Rear Center", "Rear Right", "Side Right", "Front Right" };
    
    COM0COMUtil mCOM0COMUtil;
    GameProfilesUtil mGameProfilesUtil;
    public CiliaInformationUtil mCiliaInformationUtil;

    // Use this for initialization
    void Start()
    {
        mCOM0COMUtil = new COM0COMUtil(this);
        mGameProfilesUtil = new GameProfilesUtil(this);
        mCiliaInformationUtil = new CiliaInformationUtil(this);
        
        //make a group of some gameobjects
        smells.Add(Smell1);
        smells.Add(Smell2);
        smells.Add(Smell3);
        smells.Add(Smell4);
        smells.Add(Smell5);
        smells.Add(Smell6);

        if (!File.Exists("C:\\Program Files (x86)\\com0com\\uninstall.exe"))
        {
            LaunchFakeCiliaBText.text = "Install Com-0-Com";
        }

        RefreshPorts();
        Thread ciliaReadyThread = new Thread(doCheckForCiliaReady2);
        ciliaReadyThread.Start();
    }

    public static void SetLightSpecificCilia(string ciliaNumber, uint lightNumber, uint red, uint green, uint blue)
    {
        if (lightNumber > 6)
        {
            lightNumber = 6;
        }
        if (red > byte.MaxValue)
        {
            red = byte.MaxValue;
        }
        if (green > byte.MaxValue)
        {
            green = byte.MaxValue;
        }
        if (blue > byte.MaxValue)
        {
            blue = byte.MaxValue;
        }

        string toSend = "[Specific" + ciliaNumber + ",N" + lightNumber.ToString("D1") + red.ToString("D3") + green.ToString("D3") + blue.ToString("D3") + "]";
        SendMessageToCilia(toSend);

        int ciliaNum =int.Parse(ciliaNumber);
        ciliaContents[ciliaNum][6+lightNumber] = red.ToString("D3") + green.ToString("D3") + blue.ToString("D3");
    }

    public static void deluminateCilia(string oldCilia)
    {
        string deluminateMessage = "[!#Deluminate," + oldCilia + "]";
        SendMessageToCilia(deluminateMessage);
    }

    public static void SetFanSpecificCilia(string ciliaNumber, uint fanNumber, uint fanSpeed)
    {
        if(fanNumber > 6)
        {
            fanNumber = 6;
        }
        if(fanSpeed > byte.MaxValue)
        {
            fanSpeed = byte.MaxValue;
        }
        string toSend = "[Specific" + ciliaNumber + ",F" + fanNumber.ToString("D1") + fanSpeed.ToString("D3") + "]";
        SendMessageToCilia(toSend);
    }

    public void GetCiliaInformation()
    {
        SendMessageToCilia("[!#GetLibrary]");
        string smellLibraryString = streamReader.ReadLine();
        smellLibraryClass.SubInit(smellLibraryString);


        SendMessageToCilia("[!#GetSmells]");
        Debug.Log("Sent GetSmells");
        smellLibraryString = "";

        CiliaStream.ReadTimeout = 1000;

        for (int i = 0; i < ciliaPositions.Length; i++)
        {
            ciliaPositions[i].Clear();
            ciliaPositionsS[i].Clear();
        }
        try
        {
            int count = 0;
            do
            {
                count = CiliaStream.Read(buffer, 0, buffer.Length);

                smellLibraryString += System.Text.Encoding.Default.GetString(buffer);
                Debug.Log(buffer[0]);
                if (smellLibraryString.Contains("]"))
                    break;
            } while (count != 0);
            Debug.Log("Read some bytes\n");
            Debug.Log(smellLibraryString);
            smellLibraryString = smellLibraryString.Split('[')[1];
            smellLibraryString = smellLibraryString.Split(']')[0];
            smellLibrary = smellLibraryString.Split(',', '\n');
            //List<string> frontCenterList = new List<string> { };
            ciliaSelectionDropDown.options.Clear();
            for (int i = 0; i < smellLibrary.Length; i += 14)
            {
                int ciliaIndex = int.Parse(smellLibrary[i]);
                ciliaContents[ciliaIndex][0] = smellLibrary[i + 1];
                int scentGroupForIndex = int.Parse(ciliaContents[ciliaIndex][0]);
                ciliaPositions[scentGroupForIndex].Add(ciliaIndex);
                ciliaPositionsS[scentGroupForIndex].Add(smellLibrary[i]);
                //Smells
                ciliaContents[ciliaIndex][1] = smellLibrary[i + 2];
                ciliaContents[ciliaIndex][2] = smellLibrary[i + 3];
                ciliaContents[ciliaIndex][3] = smellLibrary[i + 4];
                ciliaContents[ciliaIndex][4] = smellLibrary[i + 5];
                ciliaContents[ciliaIndex][5] = smellLibrary[i + 6];
                ciliaContents[ciliaIndex][6] = smellLibrary[i + 7];
                //colors
                ciliaContents[ciliaIndex][7] = smellLibrary[i + 8];
                ciliaContents[ciliaIndex][8] = smellLibrary[i + 9];
                ciliaContents[ciliaIndex][9] = smellLibrary[i + 10];
                ciliaContents[ciliaIndex][10] = smellLibrary[i + 11];
                ciliaContents[ciliaIndex][11] = smellLibrary[i + 12];
                ciliaContents[ciliaIndex][12] = smellLibrary[i + 13];
                string deluminateMessage = "[!#Deluminate," + ciliaIndex + "]";
                SendMessageToCilia(deluminateMessage);
            }

            //sort groups 0 - 7
            for (int orgi = 0; orgi < ciliaPositions.Length; orgi++)
            {
                ciliaPositions[orgi].Sort();
                ciliaPositionsS[orgi].Sort();
            }
            //first group to drop down menu
            ciliaSelectionDropDown.ClearOptions();
            ciliaSelectionDropDown.AddOptions(ciliaPositionsS[0]);

            Debug.Log("Made it thus far");
            for (int ai = 0; ai < ciliaPositions.Length; ai++)
                for (int ij = 0; ij < ciliaPositions[ai].Count; ij++)
                {
                    Debug.Log(ciliaPositions[ai][ij].ToString());
                    //for (int jk = 0; jk < 7; jk++)
                    //{
                    //    Debug.Log(ciliaContents[ciliaPositions[ai][ij]][jk]);
                    //}
                }
            int j;
            updateSelectable();
            for (j = 0; j < ciliaPositions.Length; j++)
            {
                if (ciliaPositions[j].Count == 0)
                {
                    continue;
                }
                else
                {
                    viewGroupDropDown.value = j;
                    ciliaSelectionDropDown.ClearOptions();
                    ciliaSelectionDropDown.AddOptions(ciliaPositionsS[j]);
                    break;//break after finding first list
                }
            }
            if (ciliaPositions.Length > 0)
            {
                List<string> contents = smellLibraryClass.getSmellLibraryContents();
                string selectedCilia = ciliaPositionsS[j][0];//get first cilia name
                oldCilia = selectedCilia;
                int cilianumber = int.Parse(selectedCilia.Replace("COM", ""));
                //search through smells which is why we stop at 7
                for (int it = 1; it < 7; it++)
                {

                    string content = ciliaContents[cilianumber][it];
                    int key = contents.BinarySearch(content);
                    smells[it - 1].value = key;
                }
            }
        }
        catch
        {
            Debug.Log("Failed to read bytes\n");
            ciliaSelectionDropDown.ClearOptions();
        }
    }

    public static void SendMessageToCilia(string messageToSend)
    {
        message = System.Text.Encoding.ASCII.GetBytes(messageToSend);

        CiliaStream.Write(message, 0, message.Length);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (timeForUpdate == true)
        {
            RefreshPorts();
            timeForUpdate = false;
        }
    }

    public void ChangeCiliaList()
    {
        ciliaSelectionDropDown.ClearOptions();
        switch (viewGroupDropDown.value)
        {
            case 0:
                ciliaSelectionDropDown.AddOptions(ciliaPositionsS[0]);
                break;
            case 1:
                ciliaSelectionDropDown.AddOptions(ciliaPositionsS[1]);
                break;
            case 2:
                ciliaSelectionDropDown.AddOptions(ciliaPositionsS[2]);
                break;
            case 3:
                ciliaSelectionDropDown.AddOptions(ciliaPositionsS[3]);
                break;
            case 4:
                ciliaSelectionDropDown.AddOptions(ciliaPositionsS[4]);
                break;
            case 5:
                ciliaSelectionDropDown.AddOptions(ciliaPositionsS[5]);
                break;
            case 6:
                ciliaSelectionDropDown.AddOptions(ciliaPositionsS[6]);
                break;
            case 7:
                ciliaSelectionDropDown.AddOptions(ciliaPositionsS[7]);
                break;
            default:
                break;
        }
        ChangeCilia();
    }

    public void ChangeCilia()
    {
        List<string> contents = smellLibraryClass.getSmellLibraryContents();
        string selectedCilia = ciliaSelectionDropDown.GetComponentInChildren<Text>().text;
        deluminateCilia(oldCilia);
        oldCilia = selectedCilia;

        if (selectedCilia.Equals(""))
        {
            uiViewGroup1.SetActive(false);
            models.SetActive(false);
            noCiliaSelectedText.SetActive(true);
        }
        else
        {
            uiViewGroup1.SetActive(true);
            models.SetActive(true);
            noCiliaSelectedText.SetActive(false);
        
            int cilianumber = int.Parse(selectedCilia);
            for (int it = 1; it < 7; it++)
            {

                string content = ciliaContents[cilianumber][it];
                int key = contents.BinarySearch(content);
                
                smells[it - 1].value = key;
            }
            //Update ColorPicker
            colorPicker1.UpdateColorPicker(ciliaContents[cilianumber][7]);
            colorPicker2.UpdateColorPicker(ciliaContents[cilianumber][8]);
            colorPicker3.UpdateColorPicker(ciliaContents[cilianumber][9]);
            colorPicker4.UpdateColorPicker(ciliaContents[cilianumber][10]);
            colorPicker5.UpdateColorPicker(ciliaContents[cilianumber][11]);
            colorPicker6.UpdateColorPicker(ciliaContents[cilianumber][12]);
        }
    }

    public void ChangeSmell(int selectorNumber)
    {
        string selectedCilia = ciliaSelectionDropDown.GetComponentInChildren<Text>().text;
        try
        {
            int ciliaNumber = int.Parse(selectedCilia);
            string smellString = smells[selectorNumber - 1].GetComponentInChildren<Text>().text;
            ciliaContents[ciliaNumber][selectorNumber] = smellString;
            SendMessageToCilia("[!#SetSmell|" + ciliaNumber+"|"+selectorNumber+"|"+smellString+"]");
        }
        catch
        { }
    }

    public void ChangeGroup(int ciliaNumber, int groupNumber)
    {
        string selectedCilia = ciliaSelectionDropDown.GetComponentInChildren<Text>().text;
        try
        {
            SendMessageToCilia("[!#SetGroup|" + ciliaNumber + "|" + groupNumber + "]");
        }
        catch
        { }
    }

    public void ChangeSurroundGroup()
    {
        int oldGroup = viewGroupDropDown.GetComponent<Dropdown>().value;
        string selectedCilia = ciliaSelectionDropDown.GetComponentInChildren<Text>().text;
        int ciliaNumber = int.Parse(selectedCilia);
        int surroundIndex = changeSurroundGroupDropDown.value;
        //DropdownA.items[0].Attributes.Add("disabled", "disabled");
        ciliaContents[ciliaNumber][0] = surroundIndex.ToString();

        int removeIndex = ciliaPositions[oldGroup].BinarySearch(ciliaNumber);
        ciliaPositions[oldGroup].RemoveAt(removeIndex);
        ciliaPositionsS[oldGroup].Remove(ciliaPositionsS[oldGroup][removeIndex]);

        ciliaPositions[surroundIndex].Add(ciliaNumber);
        ciliaPositions[surroundIndex].Sort();
        ciliaPositionsS[surroundIndex].Add(ciliaNumber.ToString());
        ciliaPositionsS[surroundIndex].Sort();

        
        ChangeGroup(ciliaNumber, surroundIndex);

        viewGroupDropDown.value = surroundIndex;
        ciliaSelectionDropDown.ClearOptions();
        ciliaSelectionDropDown.AddOptions(ciliaPositionsS[surroundIndex]);
        ciliaSelectionDropDown.value = ciliaPositionsS[surroundIndex].BinarySearch(ciliaNumber.ToString());

        ChangeCilia();
        updateSelectable();
    }

    public void SaveLights()
    {
        string selectedCilia = ciliaSelectionDropDown.GetComponentInChildren<Text>().text;
        try
        {
            int ciliaNumber = int.Parse(selectedCilia);
            string[] subC = ciliaContents[ciliaNumber];
            SendMessageToCilia("[!#SetLights|" + ciliaNumber + "|" + subC[7] + "|" + subC[8] + "|" + subC[9] + "|" + subC[10] + "|" + subC[11] + "|" + subC[12] + "]");
        }
        catch
        { }
    }

    public void SetFanBySlider(int FanNumber)
    {
        string selectedCilia = ciliaSelectionDropDown.GetComponentInChildren<Text>().text;
        switch (FanNumber)
        {
            case 1:
                SetFanSpecificCilia(selectedCilia, (uint)FanNumber, (uint)Fan1Slider.value);
                fanrpm1.setFanSpeed(Fan1Slider.value);
                break;
            case 2:
                SetFanSpecificCilia(selectedCilia, (uint)FanNumber, (uint)Fan2Slider.value);
                fanrpm2.setFanSpeed(Fan2Slider.value);
                break;
            case 3:
                SetFanSpecificCilia(selectedCilia, (uint)FanNumber, (uint)Fan3Slider.value);
                fanrpm3.setFanSpeed(Fan3Slider.value);
                break;
            case 4:
                SetFanSpecificCilia(selectedCilia, (uint)FanNumber, (uint)Fan4Slider.value);
                fanrpm4.setFanSpeed(Fan4Slider.value);
                break;
            case 5:
                SetFanSpecificCilia(selectedCilia, (uint)FanNumber, (uint)Fan5Slider.value);
                fanrpm5.setFanSpeed(Fan5Slider.value);
                break;
            case 6:
                SetFanSpecificCilia(selectedCilia, (uint)FanNumber, (uint)Fan6Slider.value);
                fanrpm6.setFanSpeed(Fan6Slider.value);
                break;
            default:
                break;
        }
        
    }

    public void RefreshPorts()
    {
        Debug.Log("refreshing Ports");

        //
        if (!CiliaClient.Connected)
        try
        {
            CiliaClient = new TcpClient();
            CiliaClient.Connect(CiliaIP, CiliaPort);
            CiliaStream = CiliaClient.GetStream();
            streamReader = new StreamReader(CiliaStream);
            Debug.Log("Cilia Connected");
        }
        catch
        {
            Debug.Log("Cilia Not Connected");
            return;
        }

        ciliaPositions = new List<int>[NUMBER_OF_SUROUNDPOSITIONS];
        ciliaPositionsS = new List<string>[NUMBER_OF_SUROUNDPOSITIONS];
        ciliaContents = new string[MAX_NUMBER_LOCAL_CILIAS][];

        for (int cPI = 0; cPI < NUMBER_OF_SUROUNDPOSITIONS; cPI++)
        {
            ciliaPositions[cPI] = new List<int>();
            ciliaPositionsS[cPI] = new List<string>();
        }
        for (int cCI = 0; cCI < MAX_NUMBER_LOCAL_CILIAS; cCI++)
        {
            ciliaContents[cCI] = new string[13];
        }
        Debug.Log("SetupGameProfiles");
        mGameProfilesUtil.SetupGameProfiles();
        GetCiliaInformation();
        ChangeCilia();
    }

    

    public void SetGameProfile()
    {
        mGameProfilesUtil.SetGameProfile();
    }

    public void LaunchFakeCilia()
    {
        if (!File.Exists("C:\\Program Files (x86)\\com0com\\uninstall.exe"))
        {
            mCOM0COMUtil.InstallCom0Com();
        }
        else{
            System.Diagnostics.ProcessStartInfo fakeCiliaProcess = new System.Diagnostics.ProcessStartInfo();
            string directory = Directory.GetCurrentDirectory();
            if (Application.isEditor)
            {
                fakeCiliaProcess.FileName = directory + "\\Builds\\8_22_19\\Fake Cilia\\FakeCilia.exe";
            }
            else
                fakeCiliaProcess.FileName = directory + "/Fake Cilia/FakeCilia.exe";
            System.Diagnostics.Process.Start(fakeCiliaProcess);

            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Cilia"))
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Cilia");
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Cilia\\CiliaTemp.dat"))
            {
                FileStream fileStream = File.Create(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Cilia\\CiliaTemp.dat");
                Byte[] Buffer = new UTF8Encoding(true).GetBytes("BUFFER");
                // Add some information to the file.
                fileStream.Write(Buffer, 0, Buffer.Length);
                fileStream.Close();
            }
            using (MemoryMappedFile file = MemoryMappedFile.CreateFromFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Cilia\\CiliaTemp.dat"))
            using (MemoryMappedViewAccessor accessor = file.CreateViewAccessor())
            {
                accessor.Write(0, (byte)0);
            }

            //Thread ciliaReadyThread = new Thread(doCheckForCiliaReady);
            //ciliaReadyThread.Start();
        }
    } 

    public void updateSelectable()
    {
        Selectable.inactiveList.Clear();
        for (int j = 0; j < surroundPositions.Length; j++)
        {
            if (ciliaPositions[j].Count == 0)
            {
                Selectable.inactiveList.Add(surroundPositions[j]);
                continue;
            }
        }
    }
    public void factoryReset()
    {
        mGameProfilesUtil.factoryReset();
    }

    public void deleteProfile()
    {
        mGameProfilesUtil.deleteProfile();
    }

    //public void doCheckForCiliaReady()
    //{
    //    byte result = 0;
    //    while (result != 255)
    //    {
    //        using (MemoryMappedFile ciliaReadyFile = MemoryMappedFile.CreateFromFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Cilia\\CiliaTemp.dat"))
    //        using (MemoryMappedViewAccessor accessor = ciliaReadyFile.CreateViewAccessor())
    //        {
    //            result = accessor.ReadByte(0);
    //            if (result == 255)
    //            {
    //                timeForUpdate = true;
    //                break;
    //            }
    //        }
    //        Thread.Sleep(3000);
    //    }
    //}

    public void doCheckForCiliaReady2()
    {
        while (continueThreads == true)
        {
            using (NamedPipeServerStream ciliaPipeServer =
            new NamedPipeServerStream("ciliaControlPannelPipe", PipeDirection.In))
            {
                ciliaPipeServer.WaitForConnection();
                using (StreamReader streamReader = new StreamReader(ciliaPipeServer))
                {
                    string ciliaMessage;
                    while ((ciliaMessage = streamReader.ReadLine()) != null)
                    {
                        if (ciliaMessage.Equals("Refresh"))
                        {
                            timeForUpdate = true;
                        }
                    }
                }
                ciliaPipeServer.Close();
            }
        }
    }

    /**
     * 
     **/
    public void UninstallCom0Com()
    {
        mCOM0COMUtil.UninstallCom0Com();
    }
    /*
     * Allow other classes to use stream reader
     */
    public String StreamReaderReadLine()
    {
        return streamReader.ReadLine();
    }

    public void StreamReaderRead(byte[] buffer, int offset, int size)
    {
        CiliaStream.Read(buffer, offset, size);
    }

    public void SetReaderTimeOut(int aTime)
    {
        CiliaStream.ReadTimeout = aTime;
    }
    /**
     * Cleans up streams.
     **/
    void OnApplicationQuit()
    {
        continueThreads = false;
        Debug.Log("closing client\n");
        if (CiliaStream != null)
            CiliaStream.Close();
        if (CiliaClient != null)
            CiliaClient.Close();
    }
    /*Working on adding 1 cilia at a time when interrupted by sdk*/
    public void AddCiliaInformation(string smellLibraryString)
    {
        smellLibrary = smellLibraryString.Split(',', '\n');

        int ciliaIndex = int.Parse(smellLibrary[0]);
        ciliaContents[ciliaIndex][0] = smellLibrary[1];
        int scentGroupForIndex = int.Parse(ciliaContents[ciliaIndex][0]);
        ciliaPositions[scentGroupForIndex].Add(ciliaIndex);
        ciliaPositionsS[scentGroupForIndex].Add(smellLibrary[0]);
        ciliaPositions[scentGroupForIndex].Sort();
        ciliaPositionsS[scentGroupForIndex].Sort();
        //Smells
        ciliaContents[ciliaIndex][1] = smellLibrary[2];
        ciliaContents[ciliaIndex][2] = smellLibrary[3];
        ciliaContents[ciliaIndex][3] = smellLibrary[4];
        ciliaContents[ciliaIndex][4] = smellLibrary[5];
        ciliaContents[ciliaIndex][5] = smellLibrary[6];
        ciliaContents[ciliaIndex][6] = smellLibrary[7];
        //colors
        ciliaContents[ciliaIndex][7] = smellLibrary[8];
        ciliaContents[ciliaIndex][8] = smellLibrary[9];
        ciliaContents[ciliaIndex][9] = smellLibrary[10];
        ciliaContents[ciliaIndex][10] = smellLibrary[11];
        ciliaContents[ciliaIndex][11] = smellLibrary[12];
        ciliaContents[ciliaIndex][12] = smellLibrary[13];
        string deluminateMessage = "[!#Deluminate," + ciliaIndex + "]";
        SendMessageToCilia(deluminateMessage);

        //first group to drop down menu
        if(changeSurroundGroupDropDown.value == scentGroupForIndex)
        {
            ciliaSelectionDropDown.ClearOptions();
            ciliaSelectionDropDown.AddOptions(ciliaPositionsS[scentGroupForIndex]);
        }
        updateSelectable();
    }
    /*working on removing once cilia at a time when interrupted by sdk*/
    public void RemoveCiliaInformation(int ciliaIndex)
    {
        int scentGroupForIndex = int.Parse(ciliaContents[ciliaIndex][0]);
        ciliaPositions[scentGroupForIndex].Remove(ciliaIndex);
        ciliaPositionsS[scentGroupForIndex].Remove(ciliaIndex.ToString());

        //first group to drop down menu
        if (changeSurroundGroupDropDown.value == scentGroupForIndex)
        {
            ciliaSelectionDropDown.ClearOptions();
            ciliaSelectionDropDown.AddOptions(ciliaPositionsS[scentGroupForIndex]);
        }
        updateSelectable();
        ChangeCilia();
    }
}

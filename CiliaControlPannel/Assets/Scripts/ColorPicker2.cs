﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPicker2 : MonoBehaviour
{
    public Slider red;
    public Slider green;
    public Slider blue;
    public InputField redInputField;
    public InputField greenInputField;
    public InputField blueInputField;
    public Image ImageColor;
    public Material Emmissive;
    public Light pointLight;
    private const int RED_OFFSET = 0;
    private const int GREEN_OFFSET = 3;
    private const int BLUE_OFFSET = 6;
    private const int COLOR_SIZE = 3;
    uint redValue255;
    uint greenValue255;
    uint blueValue255;
    /*
     * Used when we change what Cilia we are looking at.
     * 
     * Takes in a string of what color to set the color picker and sets the color picker accordingly
     */
    public void UpdateColorPicker(string color)
    {
        //Try catch just in case somehow an improper string finds its way in
        try
        {
            //Gets the rgb values from the string using the known offsets and sizes within the color string.
            uint redValue255 = uint.Parse(color.Substring(RED_OFFSET, COLOR_SIZE));
            uint greenValue255 = uint.Parse(color.Substring(GREEN_OFFSET, COLOR_SIZE));
            uint blueValue255 = uint.Parse(color.Substring(BLUE_OFFSET, COLOR_SIZE));
            //Need to normalize the value for the slider.
            float redFValue = (float)redValue255 / (float)byte.MaxValue;
            float greenFValue = (float)greenValue255 / (float)byte.MaxValue;
            float blueFValue = (float)blueValue255 / (float)byte.MaxValue;
            //get the string version
            redInputField.text = redValue255.ToString();
            greenInputField.text = greenValue255.ToString();
            blueInputField.text = blueValue255.ToString();
            //Set the slider value.
            red.value = redFValue;
            green.value = greenFValue;
            blue.value = blueFValue;
            //make a color from values
            Color tempColor = new Color(red.value, green.value, blue.value);
            //set the color of the little visualizer box near the sliders
            ImageColor.color = tempColor;
            //get an unsigned int version of the value by multiplying our normalized value by 255 and then making it an unsigned int.  will be between 0 and 255
            //set the color of the appropriate part of the model as well as the point light
            Emmissive.color = tempColor;
            Emmissive.SetColor("_EmissionColor", tempColor);
            pointLight.color = tempColor;
        }
        catch
        {

        }
    }
}

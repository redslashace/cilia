﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPMAnimate : MonoBehaviour
{
    public Animator fanAnimator;

    public void setFanSpeed(float inFanSpeed)
    {
        fanAnimator.SetFloat("Speed", inFanSpeed);
        Debug.Log("FanSpeed:" + inFanSpeed);
    }
}

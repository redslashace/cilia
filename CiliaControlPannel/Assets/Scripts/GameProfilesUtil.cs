﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameProfilesUtil
{
    Cilia mParent;
    static List<string> mCiliaGameProfiles = new List<string>();

    public GameProfilesUtil(Cilia aParent)
    {
        mParent = aParent;
    }

    public void SetGameProfile()
    {
        Cilia.SendMessageToCilia("[!#LoadProfile|" + mParent.GameProfilesDropDown.GetComponentInChildren<Text>().text + "]");
        Cilia.SendMessageToCilia("[!#GetGroupNames]");
        string groupNames = mParent.StreamReaderReadLine();
        mParent.surroundPositions = groupNames.Split(',');
        List<string> groupsList = new List<string>();

        foreach (string group in mParent.surroundPositions)
        {
            groupsList.Add(group);
        }
        mParent.viewGroupDropDown.ClearOptions();
        mParent.viewGroupDropDown.AddOptions(groupsList);
        mParent.viewGroupDropDown.value = 0;
        mParent.changeSurroundGroupDropDown.ClearOptions();
        mParent.changeSurroundGroupDropDown.AddOptions(groupsList);
        mParent.changeSurroundGroupDropDown.value = 0;

        mParent.GetCiliaInformation();
        mParent.ChangeCilia();
    }

    public void SetupGameProfiles()
    {
        Cilia.SendMessageToCilia("[!#GetProfiles]");
        string profileString = mParent.StreamReaderReadLine();
        string[] profiles = profileString.Replace("[", "").Replace("]", "").Split(',');

        mParent.GameProfilesDropDown.ClearOptions();
        mCiliaGameProfiles.Clear();
        for (int i = 0; i < profiles.Length - 1; i++)
            mCiliaGameProfiles.Add(profiles[i]);
        mCiliaGameProfiles.Sort();
        mParent.GameProfilesDropDown.AddOptions(mCiliaGameProfiles);
        mParent.GameProfilesDropDown.value = mCiliaGameProfiles.BinarySearch(profiles[profiles.Length - 1]);
    }

    public void factoryReset()
    {
        Cilia.SendMessageToCilia("[!#FactoryReset]");
        sharedDelete();
    }

    public void deleteProfile()
    {
        Cilia.SendMessageToCilia("[!#DeleteProfile]");
        sharedDelete();
    }

    private void sharedDelete()
    {
        byte[] throwawaybuffer = new byte[1];
        mParent.SetReaderTimeOut(100000);
        mParent.StreamReaderRead(throwawaybuffer, 0, 1);
        SetupGameProfiles();
        if (mParent.GameProfilesDropDown.value == 0)
        {
            SetGameProfile();
        }
        else
            mParent.GameProfilesDropDown.value = 0;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class COM0COMUtil 
{
    Cilia mParent;
    public COM0COMUtil(Cilia aParent)
    {
        mParent = aParent;
    }

    public void InstallCom0Com()
    {
        string setupFile = "cd \"com0com-3.0.0.0-i386-and-x64-signed\"\nSetup_com0com_v3.0.0.0_W7_x64_signed.exe /S";
        System.IO.File.WriteAllText("COM0COMSetup.bat", setupFile);
        System.Diagnostics.ProcessStartInfo setupProcess = new System.Diagnostics.ProcessStartInfo();
        string cdirectory = Directory.GetCurrentDirectory();
        setupProcess.FileName = cdirectory + "\\COM0COMSetup.bat";
        var sProcess = System.Diagnostics.Process.Start(setupProcess);
        sProcess.WaitForExit();
        mParent.restartComputer.SetActive(true);
    }

    public void UninstallCom0Com()
    {
        string setupFile = "cd \"C:\\Program Files (x86)\\com0com\"\nuninstall.exe /S";
        System.IO.File.WriteAllText("COM0COMSetup.bat", setupFile);
        System.Diagnostics.ProcessStartInfo setupProcess = new System.Diagnostics.ProcessStartInfo();
        string directory = Directory.GetCurrentDirectory();
        setupProcess.FileName = directory + "\\COM0COMSetup.bat";
        var sProcess = System.Diagnostics.Process.Start(setupProcess);
        sProcess.WaitForExit();
        mParent.LaunchFakeCiliaBText.text = "Install Com-0-Com";
    }
}

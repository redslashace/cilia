﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPM : MonoBehaviour
{
    float rpm6 = 0;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up, rpm6 * Time.deltaTime);
    }

    public void setFanSpeed(float inFanSpeed)
    {
        if (inFanSpeed < 0.0f)
            inFanSpeed = 0.0f;
        if (inFanSpeed > (float)byte.MaxValue)
            inFanSpeed = (float)byte.MaxValue;
        rpm6 = 141.0f * inFanSpeed; //6*6000 / 255
    }

}

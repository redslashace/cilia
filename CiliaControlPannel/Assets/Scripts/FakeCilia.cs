﻿using System;
using System.IO;
using System.IO.Ports;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using System.IO.MemoryMappedFiles;
using System.IO.Pipes;

public class FakeCilia : MonoBehaviour
{
    public RPMAnimate RPM1;
    public RPMAnimate RPM2;
    public RPMAnimate RPM3;
    public RPMAnimate RPM4;
    public RPMAnimate RPM5;
    public RPMAnimate RPM6;
    private RPMAnimate[] rpms = new RPMAnimate[6];
    public ColorPicker2 colorPicker1;
    public ColorPicker2 colorPicker2;
    public ColorPicker2 colorPicker3;
    public ColorPicker2 colorPicker4;
    public ColorPicker2 colorPicker5;
    public ColorPicker2 colorPicker6;
    private ColorPicker2[] colorPickers = new ColorPicker2[6];
    char mychar;
    char oldchar;
    bool keepAlive = true;
    string[] Fans = new string[6];
    string[] Neopixels = new string[7];
    SerialPort COMX;
    public InputField inputField;
    bool success = false;
    byte[] Buffer = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    string[] fan = { "000", "000", "000", "000", "000", "000" };
    string[] oldfan = { "", "", "", "", "", "" };
    string[] light = { "000000000", "000000000", "000000000", "000000000", "000000000", "000000000", "000000000" };
    string[] oldlight = { "", "", "", "", "", "" };
    int comInt;
    // Start is called before the first frame update
    void Start()
    {
        COMX = new SerialPort();
        rpms[0] = RPM1;
        rpms[1] = RPM2;
        rpms[2] = RPM3;
        rpms[3] = RPM4;
        rpms[4] = RPM5;
        rpms[5] = RPM6;
        colorPickers[0] = colorPicker1;
        colorPickers[1] = colorPicker2;
        colorPickers[2] = colorPicker3;
        colorPickers[3] = colorPicker4;
        colorPickers[4] = colorPicker5;
        colorPickers[5] = colorPicker6;
        for (int i = 0; i < 6; i++)
        {
            Fans[i] = "000";
            Neopixels[i] = "000,000,000";
        }
    }

    public void SetComPort()
    {

        try
        {
            //"\\\\.\\" +
            string[] comportNames = SerialPort.GetPortNames();
            List<string> comportList = new List<string>(comportNames);

            if (inputField.text.Equals(""))
                inputField.text = "0";


            comInt = int.Parse(inputField.text);
            string comstring = "COM" + comInt;
            string fcstring = "COMFC" + comInt;
            if (comportList.Contains(comstring) && !comportList.Contains(fcstring))
            {
                Debug.Log("Com port in use");
                return; //this is the case where there is a physical cilia using the port
            }
            else if (!comportList.Contains(fcstring))
            {
                CreateNewPair(comInt); //this is the case where no one is using the port and a virtual port has not been created
            }

            COMX.PortName = fcstring;
            COMX.BaudRate =  19200;
            COMX.ReadTimeout = 500;
            COMX.WriteTimeout = 500;
            COMX.Open();
            success = true;
            Thread comThread = new Thread(DoReadCom);
            comThread.Start();
        }
        catch
        {
            if (COMX.IsOpen)
                COMX.Close();
            Debug.Log("COM Port Does Not Exist");
            //Console.WriteLine("COM Port Does Not Exist Please Try Again!");
        }
        
    }

    public void Confirm()
    {
        using (NamedPipeClientStream ciliaClient =
            new NamedPipeClientStream(".", "ciliapipe", PipeDirection.Out))
        {
            ciliaClient.Connect();
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(ciliaClient))
                {
                    streamWriter.AutoFlush = true;
                    streamWriter.WriteLine("COM"+comInt+",Attach");
                    streamWriter.Close();
                }
            }
            catch (IOException e)
            {
            }

        }
        //using (MemoryMappedFile file = MemoryMappedFile.CreateFromFile(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Cilia\\CiliaTemp.dat"))
        //using (MemoryMappedViewAccessor accessor = file.CreateViewAccessor())
        //{
        //    accessor.Write(0, (byte)255);
        //    Debug.Log("Success");
        //}
        
    }

    public void FixComNumber()
    {
        //if there is an active Cilia. Shouldn't need this check but it is a good extra measure.
        //If empty that means the person pressed backspace. Just return
        if (inputField.text.Equals(""))
                return;
        //get rid of negative symbol since we don't want negative
        inputField.text = inputField.text.Replace("-", "");
            //OK now that we are past the first two checks we need to get or value to make sure it is less than 255
        int inputFieldint = int.Parse(inputField.text);
        //if value is greater than 255 remove the last character by using / by 10.
        if (inputFieldint > byte.MaxValue)
        {
            inputFieldint = inputFieldint / 10;
            inputField.text = inputFieldint.ToString();
        }
    }

    void Update()
    {
        if (success == true)
        {
            for (int i = 0; i < fan.Length; i++)
            {
                if(!fan[i].Equals(oldfan[i]))
                {
                    rpms[i].setFanSpeed(float.Parse(fan[i]));
                    oldfan[i] = fan[i];
                }
                if (!light[i].Equals(oldlight[i]))
                {
                    colorPickers[i].UpdateColorPicker(light[i]);
                    oldlight[i] = light[i];
                }
                //rpms[Buffer[0] - 49].setFanSpeed(float.Parse("" + (char)Buffer[1] + (char)Buffer[2] + (char)Buffer[3]));
                //colorPickers[Buffer[0] - 49].UpdateColorPicker("" + (char)Buffer[1] + (char)Buffer[2] + (char)Buffer[3] + (char)Buffer[4] + (char)Buffer[5] + (char)Buffer[6] + (char)Buffer[7] + (char)Buffer[8] + (char)Buffer[9]);
            }
        }
    }
    //    void UpdateFakeCilia()
    //    {
    //        int i = 0;
    //        for (i = 0; i < 6; i++)
    //            rpms[i].setFanSpeed(float.Parse(Fans[i]));

    //        for (i = 0; i < 6; i++)
    //            colorPickers[i].UpdateColorPicker(Neopixels[i]);
    //}
    void OnApplicationQuit()
    {
        keepAlive = false;
        if (COMX.IsOpen)
            COMX.Close();
        using (NamedPipeClientStream ciliaClient =
            new NamedPipeClientStream(".", "ciliapipe", PipeDirection.Out))
        {
            ciliaClient.Connect();
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(ciliaClient))
                {
                    streamWriter.AutoFlush = true;
                    streamWriter.WriteLine("COM"+comInt+",Detach");
                    streamWriter.Close();
                }
            }
            catch (IOException e)
            {
            }

        }
    }

    void DoReadCom()
    {
        while (keepAlive)
        {
            if (success == true)
            {

                //Console.Write((char)COMX.ReadChar());
                try
                {
                    switch ((char)COMX.ReadChar())
                    {
                        case 'C':
                            COMX.Write("CILIA\n");
                            break;
                        case 'F':
                            COMX.Read(Buffer, 0, 4);
                            //Fans[Buffer[0] - 49] = "" + (char)Buffer[1] + (char)Buffer[2] + (char)Buffer[3];
                            fan[Buffer[0] - 49] = "" + (char)Buffer[1] + (char)Buffer[2] + (char)Buffer[3];
                            //rpms[Buffer[0] - 49].setFanSpeed(float.Parse("" + (char)Buffer[1] + (char)Buffer[2] + (char)Buffer[3]));
                            break;
                        case 'N':
                            COMX.Read(Buffer, 0, 10);
                            try
                            {

                                //Neopixels[Buffer[0] - 49] = "" + (char)Buffer[1] + (char)Buffer[2] + (char)Buffer[3] + (char)Buffer[4] + (char)Buffer[5] + (char)Buffer[6] + (char)Buffer[7] + (char)Buffer[8] + (char)Buffer[9];
                                light[Buffer[0] - 49] = "" + (char)Buffer[1] + (char)Buffer[2] + (char)Buffer[3] + (char)Buffer[4] + (char)Buffer[5] + (char)Buffer[6] + (char)Buffer[7] + (char)Buffer[8] + (char)Buffer[9];
                                //colorPickers[Buffer[0] - 49].UpdateColorPicker("" + (char)Buffer[1] + (char)Buffer[2] + (char)Buffer[3] + (char)Buffer[4] + (char)Buffer[5] + (char)Buffer[6] + (char)Buffer[7] + (char)Buffer[8] + (char)Buffer[9]);
                            }
                            catch
                            {
                                //Console.WriteLine((char)Buffer[0]);
                            }
                            break;
                        default:
                            break;
                    }
                }
                catch
                {

                }
                //UpdateFakeCilia();
            }
        }
    }

    

    public void CreateNewPair(int comInt)
    {
        string createNewPairFile = "cd \"C:\\Program Files (x86)\\com0com\"\nsetupc install PortName=COMFC" + comInt + " PortName=COM" + comInt;
        System.IO.File.WriteAllText("CreateNewPair.bat", createNewPairFile);
        System.Diagnostics.ProcessStartInfo createNewPairProcess = new System.Diagnostics.ProcessStartInfo();
        string directory = Directory.GetCurrentDirectory();
        createNewPairProcess.FileName = directory + "\\CreateNewPair.bat";
        var createPairProcess = System.Diagnostics.Process.Start(createNewPairProcess);
        createPairProcess.WaitForExit();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SmellLibrary : MonoBehaviour
{
    public GameObject CiliaGo;
    private List<string> SmellLibraryContents = new List<string>{ "Apple", "BahamaBreeze", "CleanCotton", "Leather", "Lemon", "Rose" };
    private List<string> SmellLibraryContentsConst = new List<string> { "Apple", "BahamaBreeze", "CleanCotton", "Leather", "Lemon", "Rose" };
    string SmellLibraryName = "SmellLibrary.txt";
    string SmellLibraryPath = "SmellLibraryPath.txt";
    public Dropdown Smell1;
    public Dropdown Smell2;
    public Dropdown Smell3;
    public Dropdown Smell4;
    public Dropdown Smell5;
    public Dropdown Smell6;
    public Dropdown removeSmell;
    public InputField newSmell;
    // Start is called before the first frame update
    public void Init()
    {
        //SubInit();
    }

    public void SubInit(string smellLibrary)
    {
        //if (!File.Exists(SmellLibraryPath))
        //{
        //    System.IO.File.WriteAllText(SmellLibraryPath, SmellLibraryName);
        //}
        //else
        //{
        //    SmellLibraryName = System.IO.File.ReadAllText(SmellLibraryPath);
        //}
        //if (!File.Exists(SmellLibraryName))
        //{
        //    System.IO.File.WriteAllLines(SmellLibraryName, SmellLibraryContents.ToArray());
        //}
        //else
        //{
        //    SmellLibraryContents = new List<string>(System.IO.File.ReadAllLines(SmellLibraryName));
        //    if (SmellLibraryContents.Count == 0)
        //    {
        //        System.IO.File.WriteAllLines(SmellLibraryName, SmellLibraryContentsConst.ToArray());
        //        SmellLibraryContents = new List<string>(System.IO.File.ReadAllLines(SmellLibraryName));
        //    }
        //}
        smellLibrary = smellLibrary.TrimStart('[').TrimEnd(']');
        SmellLibraryContents = new List<string>(smellLibrary.Split(','));
        SmellLibraryContents.Sort();
        UpdateSmellSelectors();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddSmell()
    {
        string smell = newSmell.text.ToString();
        if(SmellLibraryContents.BinarySearch(smell) < 0)
        {
            SmellLibraryContents.Add(smell);
            SmellLibraryContents.Sort();
            UpdateSmellSelectors();
            System.IO.File.WriteAllLines(SmellLibraryName, SmellLibraryContents.ToArray());
            int newSmell = SmellLibraryContents.BinarySearch(smell);
            if (newSmell > -1)
            {
                if (Smell1.value >= newSmell)
                {
                    Smell1.value++;
                }
                if (Smell2.value >= newSmell)
                {
                    Smell2.value++;
                }
                if (Smell3.value >= newSmell)
                {
                    Smell3.value++;
                }
                if (Smell4.value >= newSmell)
                {
                    Smell4.value++;
                }
                if (Smell5.value >= newSmell)
                {
                    Smell5.value++;
                }
                if (Smell6.value >= newSmell)
                {
                    Smell6.value++;
                }
            }
        }
        updateRemoteSmellLibrary();
    }

    void updateRemoteSmellLibrary()
    {
        string smellsLibraryString = "[";
        for (int ai = 0; ai < SmellLibraryContents.Count; ai++)
        {
            smellsLibraryString += SmellLibraryContents[ai].ToString() + ",";
        }
        smellsLibraryString = smellsLibraryString.TrimEnd(',');
        smellsLibraryString += "]\n";
        Cilia.SendMessageToCilia("!#UpdateLibrary|" + smellsLibraryString);
    }

    public void RemoveSmell()
    {
        string smell = removeSmell.options[removeSmell.value].text;
        int removedSmell = SmellLibraryContents.BinarySearch(smell);
        SmellLibraryContents.Remove(smell);
        UpdateSmellSelectors();
        System.IO.File.WriteAllLines(SmellLibraryName, SmellLibraryContents.ToArray());
        
        if(Smell1.value == removedSmell)
        {
            CiliaGo.GetComponent<Cilia>().ChangeSmell(1);
        }
        if (Smell2.value == removedSmell)
        {
            CiliaGo.GetComponent<Cilia>().ChangeSmell(2);
        }
        if (Smell3.value == removedSmell)
        {
            CiliaGo.GetComponent<Cilia>().ChangeSmell(3);
        }
        if (Smell4.value == removedSmell)
        {
            CiliaGo.GetComponent<Cilia>().ChangeSmell(4);
        }
        if (Smell5.value == removedSmell)
        {
            CiliaGo.GetComponent<Cilia>().ChangeSmell(5);
        }
        if (Smell6.value == removedSmell)
        {
            CiliaGo.GetComponent<Cilia>().ChangeSmell(6);
        }

        if (removedSmell > -1)
        {
            if (Smell1.value > removedSmell)
            {
                Smell1.value--;
            }
            if (Smell2.value > removedSmell)
            {

                Smell2.value--;
            }
            if (Smell3.value > removedSmell)
            {
                Smell3.value--;
            }
            if (Smell4.value > removedSmell)
            {
                Smell4.value--;
            }
            if (Smell5.value > removedSmell)
            {
                Smell5.value--;
            }
            if (Smell6.value > removedSmell)
            {
                Smell6.value--;
            }
        }
        updateRemoteSmellLibrary();
    }

    private void UpdateSmellSelectors()
    {
        Smell1.ClearOptions();
        Smell2.ClearOptions();
        Smell3.ClearOptions();
        Smell4.ClearOptions();
        Smell5.ClearOptions();
        Smell6.ClearOptions();
        removeSmell.ClearOptions();
        Smell1.AddOptions(SmellLibraryContents);
        Smell2.AddOptions(SmellLibraryContents);
        Smell3.AddOptions(SmellLibraryContents);
        Smell4.AddOptions(SmellLibraryContents);
        Smell5.AddOptions(SmellLibraryContents);
        Smell6.AddOptions(SmellLibraryContents);
        removeSmell.AddOptions(SmellLibraryContents);
    }
    public List<string> getSmellLibraryContents()
    {
        return SmellLibraryContents;
    }
}

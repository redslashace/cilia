﻿using System;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Collections.Generic;
using System.Management;
using System.Linq;
using System.IO.Pipes;

namespace CiliaSDK
{

    class CiliaServer
    {
        static List<string> mPorts = new List<string>();
        const uint MAX_NUMBER_LOCAL_CILIAS = 256;//windows max is 256
        const uint NUMBER_OF_SUROUNDPOSITIONS = 256;//up to 256 groups 1 for each cilia
        const uint SIZE_OF_CILIA_CONTENTS = 13;//surround position + smells + lights
        static List<int>[] ciliaPositions = new List<int>[NUMBER_OF_SUROUNDPOSITIONS];//lists of cilias in groups

        static private string[][] ciliaContents = new string[MAX_NUMBER_LOCAL_CILIAS][];//contents of cilias

        static SerialPort[] COMX = new SerialPort[256];
        static Mutex[] token = new Mutex[256];

        static bool firstInstance = true;

        static List<string> SmellLibraryContentsConst = new List<string> { "Apple", "BahamaBreeze", "CleanCotton", "Leather", "Lemon", "Rose" };
        static List<string> SmellLibraryContents = new List<string> { "Apple", "BahamaBreeze", "CleanCotton", "Leather", "Lemon", "Rose" };



        static NetworkStream stream;
        static string CiliaDirectory;
        static string GameDirectory;
        static string SmellLibraryName;
        static string GameFolder = "Default";

        internal static void StartServer()
        {
            for (int cPI = 0; cPI < NUMBER_OF_SUROUNDPOSITIONS; cPI++)
                ciliaPositions[cPI] = new List<int>();
            for (int cCI = 0; cCI < MAX_NUMBER_LOCAL_CILIAS; cCI++)
            {
                ciliaContents[cCI] = new string[SIZE_OF_CILIA_CONTENTS];
                COMX[cCI] = new SerialPort();
                token[cCI] = new Mutex();
            }
            string AppDataDirectory = "C:/Windows/ServiceProfiles/LocalService/AppData/Roaming";
            CiliaDirectory = Path.Combine(AppDataDirectory, "Cilia");
            GameDirectory = CiliaDirectory;

            SmellLibraryName = "SmellLibrary.txt";
            SmellLibraryName = Path.Combine(CiliaDirectory, SmellLibraryName);

            GameFolder = "Default";
            GameDirectory = Path.Combine(CiliaDirectory, GameFolder);

            CreateDefaultGameProfile();




            int COMi = 0;
            TcpListener server = null;
            Int32 port = 1995;
            IPAddress localHost = IPAddress.Any;
            TcpClient client;
            int i;
            //Find all Physical Cilias that were already attached before computer started
            RefreshPortsNonBlock();
            //Setup a thread to receive when fake Cilias Attach or Detach
            Thread attachFakeCilias = new Thread(FakeCiliaEventHandler);
            attachFakeCilias.Start();
            //Setup an interrupt to receive when physical Cilias are attached.            
            WqlEventQuery ciliaAttachedQuery = new WqlEventQuery("SELECT * FROM __InstanceCreationEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_USBHub'");
            ManagementEventWatcher ciliaAttachedWatcher = new ManagementEventWatcher(ciliaAttachedQuery);
            ciliaAttachedWatcher.EventArrived += new EventArrivedEventHandler(CiliaAttachedEvent);
            ciliaAttachedWatcher.Start();
            //Setup an interrupt to receive when physical Cilias are detached.
            WqlEventQuery ciliaDettachedQuery = new WqlEventQuery("SELECT * FROM __InstanceDeletionEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_USBHub'");
            ManagementEventWatcher ciliaDettachedWatcher = new ManagementEventWatcher(ciliaDettachedQuery);
            ciliaDettachedWatcher.EventArrived += new EventArrivedEventHandler(CiliaDetachedEvent);
            ciliaDettachedWatcher.Start();


            //RefreshPorts();
            String data = null;
            String[] dataSplit;
            String[] dataSplit2;

            String processString = "";

            while (true)
            {
                try
                {
                    server = new TcpListener(localHost, port);
                    server.Start();
                    Byte[] bytes = new Byte[10000];

                    while (true)
                    {

                        client = server.AcceptTcpClient();

                        //RefreshPorts();

                        data = null;
                        stream = client.GetStream();

                        while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                            dataSplit = data.Split('[');
                            foreach (String subString in dataSplit)
                            {
                                if (subString.Contains("]"))
                                {
                                    dataSplit2 = subString.Split(']');
                                    processString = processString + dataSplit2[0];
                                    if (processString.Contains("!#")) //use shebang to denote a special processing case
                                    {
                                        SpecialProcess(processString);
                                    }
                                    else
                                    {
                                        Thread processMessage = new Thread(ProcessMessage);
                                        processMessage.Start(processString);
                                    }
                                    processString = dataSplit2[1];
                                }
                                else
                                {
                                    processString = processString + subString;
                                }
                            }
                        }
                        try
                        {
                            stream.Close();
                            client.Close();
                        }
                        catch (ObjectDisposedException obe)
                        {
                            Console.WriteLine("ObjectDisposedException");
                        }
                        for (COMi = 0; COMi < COMX.Length; COMi++)
                        {
                            WriteString(COMi, "N1" + ciliaContents[COMi][7]);
                            WriteString(COMi, "N2" + ciliaContents[COMi][8]);
                            WriteString(COMi, "N3" + ciliaContents[COMi][9]);
                            WriteString(COMi, "N4" + ciliaContents[COMi][10]);
                            WriteString(COMi, "N5" + ciliaContents[COMi][11]);
                            WriteString(COMi, "N6" + ciliaContents[COMi][12]);
                            WriteString(COMi, "F1000");
                            WriteString(COMi, "F2000");
                            WriteString(COMi, "F3000");
                            WriteString(COMi, "F4000");
                            WriteString(COMi, "F5000");
                            WriteString(COMi, "F6000");
                        }
                    }
                }
                catch (SocketException e)
                {
                    Console.WriteLine("socket exception");
                }
                catch (IOException e2)
                {
                    Console.WriteLine("IOException\n");
                }
                catch (ObjectDisposedException o)
                {
                    Console.WriteLine("ObjectDisposedException");
                }
                finally
                {
                    //client.Close();
                    try
                    {
                        server.Stop();
                    }
                    catch (ObjectDisposedException o)
                    {
                        Console.WriteLine("ObjectDisposedException");
                    }
                }
            }
        }

        public static void WriteString(int COMXi, string InputString)
        {
            try
            {
                token[COMXi].WaitOne();
                if (COMX[COMXi].IsOpen)
                    COMX[COMXi].Write(InputString);
                token[COMXi].ReleaseMutex();
            }
            catch
            {

            }
        }

        public static void RefreshPortsNonBlock()
        {
            for (int i = 0; i < 256; i++)
            {

                token[i] = new Mutex();
            }
            for (int clearI = 0; clearI < NUMBER_OF_SUROUNDPOSITIONS; clearI++)
            {
                ciliaPositions[clearI].Clear();
            }
            if (firstInstance == true)
                firstInstance = false;
            else
                for (int i = 0; i < COMX.Length; i++)
                {
                    try
                    {
                        if (COMX[i].IsOpen)
                            COMX[i].Close();
                    }
                    catch { }
                }
            int ComInt = 0;
            //string[] ports;
            string readString = "";
            int FoundCilias = 0;
            mPorts = new List<string>(SerialPort.GetPortNames());
            foreach (string port in mPorts)
            {
                if (port.Contains("FC") || !port.Contains("COM"))
                    continue;
                try
                {
                    ComInt = GetComInt(port);
                    if (ComInt == -1)
                        continue;
                    COMX[ComInt].PortName = port.ToString();
                    COMX[ComInt].BaudRate = 19200;
                    COMX[ComInt].ReadTimeout = 10;
                    COMX[ComInt].WriteTimeout = 10;
                    COMX[ComInt].Open();
                    COMX[ComInt].DiscardInBuffer();
                    COMX[ComInt].DiscardOutBuffer();
                    COMX[ComInt].Write("C");
                    Thread.Sleep(20);
                    readString = COMX[ComInt].ReadLine();
                    if (readString.Contains("CILIA"))
                    {
                        WriteString(ComInt, "N1" + ciliaContents[ComInt][7]);
                        WriteString(ComInt, "N2" + ciliaContents[ComInt][8]);
                        WriteString(ComInt, "N3" + ciliaContents[ComInt][9]);
                        WriteString(ComInt, "N4" + ciliaContents[ComInt][10]);
                        WriteString(ComInt, "N5" + ciliaContents[ComInt][11]);
                        WriteString(ComInt, "N6" + ciliaContents[ComInt][12]);
                        WriteString(ComInt, "F1000");
                        WriteString(ComInt, "F2000");
                        WriteString(ComInt, "F3000");
                        WriteString(ComInt, "F4000");
                        WriteString(ComInt, "F5000");
                        WriteString(ComInt, "F6000");
                        FoundCilias++;

                        ciliaPositions[int.Parse(ciliaContents[ComInt][0])].Add(ComInt);
                    }
                    else
                    {
                        try
                        {
                            if (COMX[ComInt].IsOpen)
                                COMX[ComInt].Close();
                        }
                        catch { }
                    }
                }
                catch
                {
                    try
                    {
                        if (COMX[ComInt].IsOpen)
                            COMX[ComInt].Close();
                    }
                    catch { }
                }
            }
        }

        static public int ReturnSmellNumber(int Cilia, string smell)
        {
            for (int i = 1; i < ciliaContents[Cilia].Length; i++)
            {
                if (ciliaContents[Cilia][i].Equals(smell))
                    return i;
            }
            return -1;
        }

        static public int ReturnSmellNumberOffset(int Cilia, string smell, int offset)
        {
            for (int i = offset; i < ciliaContents[Cilia].Length; i++)
            {
                if (ciliaContents[Cilia][i].Equals(smell))
                    return i;
            }
            return -1;
        }

        static int GetComInt(string comName)
        {
            try
            {
                return int.Parse(comName.Replace("COM", ""));
            }
            catch
            {
                return -1;
            }
        }

        static void SpecialProcess(string processString)
        {
            string surroundGroups = "";
            String[] tSplit;
            if (processString.Contains("RefreshCilias"))
            {
                RefreshPortsNonBlock();
                byte[] completeRefresh = { 99 };
                stream.Write(completeRefresh, 0, 1);
            }
            else if (processString.Contains("SetSmell"))
            {
                //processString = processString.Replace("SetSmell", "");

                tSplit = processString.Split('|');
                int tCiliaNumber = int.Parse(tSplit[1]);
                int tSlotNumber = int.Parse(tSplit[2]);
                String tValue = tSplit[3];
                ciliaContents[tCiliaNumber][tSlotNumber] = tValue;
                String tCOMNAME = "COMPORT" + tCiliaNumber.ToString();
                String tAggregate = "";
                for (int ti = 0; ti < (SIZE_OF_CILIA_CONTENTS); ti++)
                {
                    tAggregate += ciliaContents[tCiliaNumber][ti] + "\n";
                }
                tAggregate = tAggregate.TrimEnd('\n');
                System.IO.File.WriteAllText(Path.Combine(GameDirectory, tCOMNAME), tAggregate);
            }
            else if (processString.Contains("SetLights"))
            {
                //processString = processString.Replace("SetSmell", "");

                tSplit = processString.Split('|');
                int tCiliaNumber = int.Parse(tSplit[1]);
                String tSmellValue1 = tSplit[2];
                String tSmellValue2 = tSplit[3];
                String tSmellValue3 = tSplit[4];
                String tSmellValue4 = tSplit[5];
                String tSmellValue5 = tSplit[6];
                String tSmellValue6 = tSplit[7];
                ciliaContents[tCiliaNumber][7] = tSmellValue1;
                ciliaContents[tCiliaNumber][8] = tSmellValue2;
                ciliaContents[tCiliaNumber][9] = tSmellValue3;
                ciliaContents[tCiliaNumber][10] = tSmellValue4;
                ciliaContents[tCiliaNumber][11] = tSmellValue5;
                ciliaContents[tCiliaNumber][12] = tSmellValue6;
                String tCOMNAME = "COMPORT" + tCiliaNumber.ToString();
                String tAggregate = "";
                for (int ti = 0; ti < (SIZE_OF_CILIA_CONTENTS); ti++)
                {
                    tAggregate += ciliaContents[tCiliaNumber][ti] + "\n";
                }
                tAggregate = tAggregate.TrimEnd('\n');
                System.IO.File.WriteAllText(Path.Combine(GameDirectory, tCOMNAME), tAggregate);
            }
            else if (processString.Contains("SetGroup"))
            {
                //processString = processString.Replace("SetSmell", "");

                tSplit = processString.Split('|');
                int tCiliaNumber = int.Parse(tSplit[1]);
                string surroundGroup = tSplit[2];
                ciliaContents[tCiliaNumber][0] = surroundGroup;
                String tCOMNAME = "COMPORT" + tCiliaNumber.ToString();
                String tAggregate = "";
                for (int ti = 0; ti < (SIZE_OF_CILIA_CONTENTS); ti++)
                {
                    tAggregate += ciliaContents[tCiliaNumber][ti] + "\n";
                }
                tAggregate = tAggregate.TrimEnd('\n');
                System.IO.File.WriteAllText(Path.Combine(GameDirectory, tCOMNAME), tAggregate);
            }
            else if (processString.Contains("Deluminate"))
            {
                //processString = processString.Replace("SetSmell", "");
                try
                {
                    tSplit = processString.Split(',');
                    int tCiliaNumber = int.Parse(tSplit[1]);
                    WriteString(tCiliaNumber, "N1000000000");
                    WriteString(tCiliaNumber, "N2000000000");
                    WriteString(tCiliaNumber, "N3000000000");
                    WriteString(tCiliaNumber, "N4000000000");
                    WriteString(tCiliaNumber, "N5000000000");
                    WriteString(tCiliaNumber, "N6000000000");
                }
                catch
                {

                }
            }
            else if (processString.Contains("GetSmells"))
            {
                string smellsString = "[";
                for (int ai = 0; ai < NUMBER_OF_SUROUNDPOSITIONS; ai++)
                    for (int ij = 0; ij < ciliaPositions[ai].Count; ij++)
                    {
                        smellsString += ciliaPositions[ai][ij].ToString();
                        for (int jk = 0; jk < (SIZE_OF_CILIA_CONTENTS); jk++)
                        {
                            smellsString += "," + ciliaContents[ciliaPositions[ai][ij]][jk];
                        }
                        smellsString += "\n";
                    }
                smellsString = smellsString.TrimEnd('\n');
                smellsString += "]";
                byte[] smellBytes = System.Text.Encoding.UTF8.GetBytes(smellsString);
                stream.Write(smellBytes, 0, smellBytes.Length);
            }
            else if (processString.Contains("GetLibrary"))
            {
                string smellsLibraryString = "[";
                for (int ai = 0; ai < SmellLibraryContents.Count; ai++)
                {
                    smellsLibraryString += SmellLibraryContents[ai].ToString() + ",";
                }
                smellsLibraryString = smellsLibraryString.TrimEnd(',');
                smellsLibraryString += "]\n";
                byte[] smellBytes = System.Text.Encoding.UTF8.GetBytes(smellsLibraryString);
                stream.Write(smellBytes, 0, smellBytes.Length);
            }
            else if (processString.Contains("UpdateLibrary"))
            {
                SmellLibraryContents = new List<string>(processString.Split('|')[1].Split(','));
                System.IO.File.WriteAllLines(SmellLibraryName, SmellLibraryContents.ToArray());
            }
            else if (processString.Contains("AddToLibrary"))
            {
                //get a list of new smells to add to the old list of smells.
                List<string> SmellLibraryAditionalContents = new List<string>(processString.Split('|')[1].Split(','));
                //check to see if each smell is already in old list
                foreach (string newSmell in SmellLibraryAditionalContents)
                {
                    if (!SmellLibraryContents.Contains(newSmell))
                        SmellLibraryContents.Add(newSmell);
                }
                SmellLibraryContents.Sort();
                System.IO.File.WriteAllLines(SmellLibraryName, SmellLibraryContents.ToArray());
            }
            else if (processString.Contains("LoadProfile"))
            {
                //!#LoadProfile|<GameProfile>,<surroundgroup>,<Smell 1>,<Smell 2>,<Smell 3>,<Smell4>,<Smell5>,<Smell6>,<light1>,<light2>,<light3>,<light4>,<light5>,<light6>
                string ss = processString.Split('|')[1];
                string[] sS = ss.Split(',');
                //sS[0] contains game folder sS may contain the rest of the info needed
                LoadGameProfile(sS[0], sS);
            }
            else if (processString.Contains("GetProfiles"))
            {
                string[] directoryies = Directory.GetDirectories(CiliaDirectory);

                string directoryListString = "[";
                for (int ai = 0; ai < directoryies.Length; ai++)
                {
                    directoryListString += directoryies[ai].Substring(CiliaDirectory.Length + 1, directoryies[ai].Length - (CiliaDirectory.Length + 1)) + ",";
                }
                directoryListString += GameFolder;
                directoryListString += "]\n";
                byte[] directoryBytes = System.Text.Encoding.UTF8.GetBytes(directoryListString);
                stream.Write(directoryBytes, 0, directoryBytes.Length);
            }
            else if (processString.Contains("GetGroupNames"))
            {
                surroundGroups = "";
                surroundGroups = System.IO.File.ReadAllText(Path.Combine(GameDirectory, "SurroundGroups"));
                surroundGroups += "\n";
                byte[] surroundGroupsBytes = System.Text.Encoding.UTF8.GetBytes(surroundGroups);
                stream.Write(surroundGroupsBytes, 0, surroundGroupsBytes.Length);
            }
            else if (processString.Contains("FactoryReset"))
            {
                //System.IO.DirectoryInfo ciliaDirectoryInfo = new DirectoryInfo(CiliaDirectory);
                DeleteFolder(CiliaDirectory);
                CreateDefaultGameProfile();
                string[] sS = { };//no parameters since loading an existing profile.
                LoadGameProfile("Default", sS);
                byte[] completeRefresh = { 99 };
                stream.Write(completeRefresh, 0, 1);
                int i = 0;
            }
            else if (processString.Contains("DeleteProfile"))
            {
                DeleteFolder(GameDirectory);
                if (GameFolder.Equals("Default"))
                {
                    CreateDefaultGameProfile();
                }
                string[] sS = { };//no parameters since loading an existing profile.
                LoadGameProfile("Default", sS);
                byte[] completeRefresh = { 99 };
                stream.Write(completeRefresh, 0, 1);
                int i = 0;
            }
        }

        public static void DeleteFolder(string folder)
        {
            for (int i = 0; i < 1000; i++)
            {
                try
                {
                    if (Directory.Exists(folder))
                    {
                        Directory.Delete(folder, true);
                    }
                    else
                        break;
                }
                catch (IOException e)
                {
                    string exception = e.Message;
                };
            }
        }

        static void LoadGameProfile(string aGameFolder, string[] aSS)
        {
            GameFolder = aGameFolder;
            string surroundGroups = "";
            //this function will check if a game profile exists profile is directory
            GameDirectory = Path.Combine(CiliaDirectory, GameFolder);
            string ciliaContentsTemplate = "";
            if (!Directory.Exists(GameDirectory))
            {
                Directory.CreateDirectory(GameDirectory);
            }
            if (aSS.Length > 1)
            {
                //create template
                string sP = aSS[1];
                string s1 = aSS[2]; string s2 = aSS[3]; string s3 = aSS[4]; string s4 = aSS[5]; string s5 = aSS[6]; string s6 = aSS[7];
                string n1 = aSS[8]; string n2 = aSS[9]; string n3 = aSS[10]; string n4 = aSS[11]; string n5 = aSS[12]; string n6 = aSS[13];
                ciliaContentsTemplate = sP + "\n" + s1 + "\n" + s2 + "\n" + s3 + "\n" + s4 + "\n" + s5 + "\n" + s6 + "\n" +
                                                n1 + "\n" + n2 + "\n" + n3 + "\n" + n4 + "\n" + n5 + "\n" + n6;

                for (int i = 14; i < aSS.Length; i++)
                {
                    surroundGroups += aSS[i] + ",";
                }
                surroundGroups = surroundGroups.TrimEnd(',');
                System.IO.File.WriteAllText(Path.Combine(GameDirectory, "SurroundGroups"), surroundGroups);
            }


            for (int comI = 0; comI < MAX_NUMBER_LOCAL_CILIAS; comI++)
            {
                string comName = "COMPORT" + comI.ToString();
                if (!File.Exists(Path.Combine(GameDirectory, comName)))
                {
                    System.IO.File.WriteAllText(Path.Combine(GameDirectory, comName), ciliaContentsTemplate);
                }
                ciliaContents[comI] = System.IO.File.ReadAllLines(Path.Combine(GameDirectory, comName));
            }
        }

        static void ProcessMessage(object processStringObj)
        {
            string processString = processStringObj.ToString();
            String[] processStringSplit;
            try
            {
                processStringSplit = processString.Split(',');

                if (processString.Contains("G"))
                {
                    byte group = byte.Parse(processString.Split('<')[1].Split('>')[0]);
                    SubProcessMessage(group, processStringSplit, processString);
                }
                if (processString.Contains("All"))
                {
                    for (int ai = 0; ai < NUMBER_OF_SUROUNDPOSITIONS; ai++)
                        SubProcessMessage(ai, processStringSplit, processString);
                }
                else if (processString.Contains("Specific"))
                {
                    string ciliaNumberSpecific = processStringSplit[0].Replace("Specific", "");
                    int tempCiliaNumber = int.Parse(ciliaNumberSpecific);
                    WriteString(tempCiliaNumber, processStringSplit[1]);
                }
                else
                {
                    for (int ai = 0; ai < NUMBER_OF_SUROUNDPOSITIONS; ai++)
                        RawSubProcessMessage(ai, processString);
                }

            }
            catch
            {

            }
        }

        static void CreateDefaultGameProfile()
        {
            string comName = "";
            string ciliaContentsTemplate = "0\nLemon\nApple\nLeather\nCleanCotton\nBahamaBreeze\nRose\n000128064\n000128064\n000128064\n000128064\n000128064\n000128064";
            string surroundGroups = "FrontCenter,FrontLeft,SideLeft,RearLeft,RearCenter,RearRight,SideRight,FrontRight";

            if (!Directory.Exists(CiliaDirectory))
            {
                Directory.CreateDirectory(CiliaDirectory);
            }
            if (!Directory.Exists(GameDirectory))
            {
                Directory.CreateDirectory(GameDirectory);
            }

            System.IO.File.WriteAllText(Path.Combine(GameDirectory, "SurroundGroups"), surroundGroups);

            if (!File.Exists(SmellLibraryName))
            {
                System.IO.File.WriteAllLines(SmellLibraryName, SmellLibraryContentsConst.ToArray());
            }
            else
            {
                SmellLibraryContents = new List<string>(System.IO.File.ReadAllLines(SmellLibraryName));
                if (SmellLibraryContents.Count == 0)
                {
                    System.IO.File.WriteAllLines(SmellLibraryName, SmellLibraryContentsConst.ToArray());
                    SmellLibraryContents = new List<string>(System.IO.File.ReadAllLines(SmellLibraryName));
                }
            }
            for (int comI = 0; comI < MAX_NUMBER_LOCAL_CILIAS; comI++)
            {
                comName = "COMPORT" + comI.ToString();
                if (!File.Exists(Path.Combine(GameDirectory, comName)))
                {
                    System.IO.File.WriteAllText(Path.Combine(GameDirectory, comName), ciliaContentsTemplate);
                }
                ciliaContents[comI] = System.IO.File.ReadAllLines(Path.Combine(GameDirectory, comName));
            }
        }

        static void SubProcessMessage(int position, string[] processStringSplit, string processString2)
        {
            int tempCiliaNumber = 0;
            int tempSmellNumber = 0;
            for (int ij = 0; ij < ciliaPositions[position].Count; ij++)
            {
                tempCiliaNumber = ciliaPositions[position][ij];


                if (processString2.Contains("N"))
                    WriteString(tempCiliaNumber, processStringSplit[1]);
                else if (processString2.Contains("F"))
                {
                    tempSmellNumber = ReturnSmellNumberOffset(tempCiliaNumber, processStringSplit[1], 1);
                    while (tempSmellNumber != -1)
                    {
                        WriteString(tempCiliaNumber, "F" + tempSmellNumber + processStringSplit[2]);
                        tempSmellNumber = ReturnSmellNumberOffset(tempCiliaNumber, processStringSplit[1], tempSmellNumber + 1);
                    }
                }
            }
        }
        static void RawSubProcessMessage(int position, string processString2)
        {
            int tempCiliaNumber = 0;
            for (int ij = 0; ij < ciliaPositions[position].Count; ij++)
            {
                if (!COMX[tempCiliaNumber].IsOpen)
                    continue;
                tempCiliaNumber = ciliaPositions[position][ij];
                WriteString(tempCiliaNumber, processString2);
            }
        }

        static void CiliaAttachedEvent(object sender, EventArrivedEventArgs args)
        {
            List<string> ports;
            ports = new List<string>(SerialPort.GetPortNames());
            
            List<string> addedPorts = ports.Except(mPorts).ToList();
            CiliasAttach(addedPorts);
        }
        static void CiliasAttach(List<string> portsToAdd)
        {
            foreach (string port in portsToAdd)
            {
                CiliaAttach(port);
            }
        }
        static void CiliaAttach(string portToAdd)
        {
            int ComInt = 0;
            string readString = "";
            mPorts.Add(portToAdd);
            if (portToAdd.Contains("FC") || !portToAdd.Contains("COM"))
                return;
            try
            {
                ComInt = GetComInt(portToAdd);
                if (ComInt == -1)
                    return;
                COMX[ComInt].PortName = portToAdd.ToString();
                COMX[ComInt].BaudRate = 19200;
                COMX[ComInt].ReadTimeout = 10;
                COMX[ComInt].WriteTimeout = 10;
                COMX[ComInt].Open();
                COMX[ComInt].DiscardInBuffer();
                COMX[ComInt].DiscardOutBuffer();
                COMX[ComInt].Write("C");
                Thread.Sleep(20);
                readString = COMX[ComInt].ReadLine();
                if (readString.Contains("CILIA"))
                {
                    WriteString(ComInt, "N1" + ciliaContents[ComInt][7]);
                    WriteString(ComInt, "N2" + ciliaContents[ComInt][8]);
                    WriteString(ComInt, "N3" + ciliaContents[ComInt][9]);
                    WriteString(ComInt, "N4" + ciliaContents[ComInt][10]);
                    WriteString(ComInt, "N5" + ciliaContents[ComInt][11]);
                    WriteString(ComInt, "N6" + ciliaContents[ComInt][12]);
                    WriteString(ComInt, "F1000");
                    WriteString(ComInt, "F2000");
                    WriteString(ComInt, "F3000");
                    WriteString(ComInt, "F4000");
                    WriteString(ComInt, "F5000");
                    WriteString(ComInt, "F6000");

                    ciliaPositions[int.Parse(ciliaContents[ComInt][0])].Add(ComInt);
                    ForceUpdate();
                }
                else
                {
                    try
                    {
                        if (COMX[ComInt].IsOpen)
                            COMX[ComInt].Close();
                    }
                    catch { }
                }
            }
            catch
            {
                try
                {
                    if (COMX[ComInt].IsOpen)
                        COMX[ComInt].Close();
                }
                catch { }
            }
            
        }

        

        static void CiliaDetachedEvent(object sender, EventArrivedEventArgs args)
        {
            List<string> ports;
            ports = new List<string>(SerialPort.GetPortNames());

            List<string> removedPorts = mPorts.Except(ports).ToList();
            CiliasDetach(removedPorts);
        }
        static void CiliasDetach(List<string> portsToRemove)
        {
            foreach (string port in portsToRemove)
            {
                CiliaDetach(port);
            }
        }
        static void CiliaDetach(string portToRemove)
        {
            mPorts.Remove(portToRemove);
            int ComInt = GetComInt(portToRemove);
            ciliaPositions[int.Parse(ciliaContents[ComInt][0])].Remove(ComInt);
            if (COMX[ComInt].IsOpen)
                COMX[ComInt].Close();
            ForceUpdate();
        }
        static void FakeCiliaEventHandler()
        {
            while (true)
            {
                using (NamedPipeServerStream ciliaPipeServer =
                new NamedPipeServerStream("ciliapipe", PipeDirection.In))
                {
                    ciliaPipeServer.WaitForConnection();
                    using (StreamReader streamReader = new StreamReader(ciliaPipeServer))
                    {
                        string ciliaMessage;
                        while ((ciliaMessage = streamReader.ReadLine()) != null)
                        {
                            string[] ciliaMessages = ciliaMessage.Split(',');
                            if (ciliaMessages[1].Equals("Attach"))
                            {
                                CiliaAttach(ciliaMessages[0]);
                                ForceUpdate();
                            }
                            else if (ciliaMessages[1].Equals("Detach"))
                            {
                                CiliaDetach(ciliaMessages[0]);
                                ForceUpdate();
                            }
                        }
                    }
                    ciliaPipeServer.Close();
                }
            }
        }
        static void ForceUpdate()
        {
            using (NamedPipeClientStream ciliaClient =
            new NamedPipeClientStream(".", "ciliaControlPannelPipe", PipeDirection.Out))
            {
                ciliaClient.Connect();
                try
                {
                    using (StreamWriter streamWriter = new StreamWriter(ciliaClient))
                    {
                        streamWriter.AutoFlush = true;
                        streamWriter.WriteLine("Refresh");
                        streamWriter.Close();
                    }
                }
                catch (IOException e)
                {
                }

            }
        }
    }
}
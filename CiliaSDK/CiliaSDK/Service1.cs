﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CiliaSDK
{
    public partial class Service1 : ServiceBase
    {
        Thread CiliaSDKThread;
        public Service1()
        {
            InitializeComponent();
        }

        public void DebugStart()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Thread CiliaSDKThread = new Thread(CiliaServer.StartServer);
                CiliaSDKThread.Start();
                //CiliaServer.StartServer();
                //CiliaServer.StartServer();
                //System.IO.File.Create(AppDomain.CurrentDomain.BaseDirectory + "OnStart.txt");
            }
            catch
            {

            }
        }

        protected override void OnStop()
        {
            try
            {
                CiliaSDKThread.Join();

                //System.IO.File.Create(AppDomain.CurrentDomain.BaseDirectory + "OnStop.txt");
            }
            catch
            {

            }
        }


    }
}

// Fill out your copyright notice in the Description page of Project Settings.

#include "CiliaPlugin.h"

static SOCKET CiliaSocket;

void UCiliaPlugin::Light(int LightNumber, int Red, int Green, int Blue)
{
	std::string LightNumberString;
	LightNumberString = std::to_string(LightNumber);

	std::string redString;
	if (Red < 10)
		redString = "00" + std::to_string(Red);
	else if (Red < 100)
		redString = "0" + std::to_string(Red);
	else
		redString = std::to_string(Red);

	std::string greenString;
	if (Green < 10)
		greenString = "00" + std::to_string(Green);
	else if (Green < 100)
		greenString = "0" + std::to_string(Green);
	else
		greenString = std::to_string(Green);

	std::string blueString;
	if (Blue < 10)
		blueString = "00" + std::to_string(Blue);
	else if (Blue < 100)
		blueString = "0" + std::to_string(Blue);
	else
		blueString = std::to_string(Blue);

	std::string stringForCilia = "[N" + LightNumberString + redString + greenString + blueString + "]";

	send(CiliaSocket, stringForCilia.c_str(), stringForCilia.size() + 1, 0);
	return;
}

void UCiliaPlugin::Fan(int FanNumber, int FanSpeed)
{
	std::string FanNumberString;
	FanNumberString = std::to_string(FanNumber);

	std::string FanSpeedString;
	if (FanSpeed < 10)
		FanSpeedString = "00" + std::to_string(FanSpeed);
	else if (FanSpeed < 100)
		FanSpeedString = "0" + std::to_string(FanSpeed);
	else
		FanSpeedString = std::to_string(FanSpeed);

	std::string stringForCilia = "[F" + FanNumberString + FanSpeedString + "]";

	send(CiliaSocket, stringForCilia.c_str(), stringForCilia.size() + 1, 0);
	return;
}

void UCiliaPlugin::ConnectCilia()
{
	std::string CiliaIP = "127.0.0.1";
	//std::string CiliaIP = "localhost";
	int CiliaPort = 1995;

	WSADATA WindowsSocketData;
	WORD VersionRequested = MAKEWORD(2, 2);
	int WSAResult = WSAStartup(VersionRequested, &WindowsSocketData);
	if (WSAResult != 0)
	{
		return;
	}

	CiliaSocket = socket(AF_INET, SOCK_STREAM, 0);

	if (CiliaSocket == INVALID_SOCKET)
	{
		WSACleanup();
		return;
	}

	sockaddr_in CiliaSockAddr_in;
	CiliaSockAddr_in.sin_family = AF_INET;
	CiliaSockAddr_in.sin_port = htons(CiliaPort);
	inet_pton(AF_INET, CiliaIP.c_str(), &CiliaSockAddr_in.sin_addr);

	int ConnectionResult = connect(CiliaSocket, (sockaddr*)&CiliaSockAddr_in, sizeof(CiliaSockAddr_in));
	if (ConnectionResult == SOCKET_ERROR)
	{
		closesocket(CiliaSocket);
		WSACleanup();
		return;
	}
	
	return;
}

void UCiliaPlugin::DisconnectCilia()
{
	closesocket(CiliaSocket);
	WSACleanup();
	return;
}
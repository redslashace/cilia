// Fill out your copyright notice in the Description page of Project Settings.
#include <iostream>
#include <string>
#include <string.h>
#include <WS2tcpip.h>

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "CiliaPlugin.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT3_API UCiliaPlugin : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = "CiliaFunctions")
		static void Light(int LightNumber, int Red, int Green, int Glue);
	UFUNCTION(BlueprintCallable, Category = "CiliaFunctions")
		static void Fan(int FanNumber, int FanSpeed);
	UFUNCTION(BlueprintCallable, Category = "CiliaFunctions")
		static void ConnectCilia();
	UFUNCTION(BlueprintCallable, Category = "CiliaFunctions")
		static void DisconnectCilia();	
};
